package com.google.appengine.mct;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheServiceFactory;
import com.google.appengine.util.ConstantUtils;
import com.google.gdata.data.appsforyourdomain.provisioning.UserEntry;
import com.google.gdata.data.appsforyourdomain.provisioning.UserFeed;

@SuppressWarnings("serial")
public class AddSupervisor extends BaseServlet {
	
	private static final Logger log = Logger.getLogger(AddSupervisor.class);

	public static StringBuffer regionTable = new StringBuffer();
	public static MemcacheService memcache = MemcacheServiceFactory.getMemcacheService(null);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.debug(AddSupervisor.class);
		regionTable.setLength(0);
		String appAdminAcc = "";
		String appAdminPwd = "";
		String domain = "";
		String emailAddress = req.getParameter("emailAddress");
		String regionSelected[] = req.getParameterValues("region[]"); 
		String cmd = req.getParameter("cmd2");
		boolean existDirectory = false;
		String strRegion = "";
		String existedRegion = "";
		
		SettingService sr = new SettingService();
		for (Setting set : sr.getSetting()) {
				domain = set.getAppDomain();
				appAdminAcc = set.getAppAdminAcc();
				appAdminPwd = set.getAppAdminAccPass();
			
		}

			// Add supervisor and region(s)
			for (int i=0; i<regionSelected.length; i++) {
				if (i == 0) {
					strRegion = regionSelected[i].toString();
				} else {
					strRegion = strRegion + ", " + regionSelected[i].toString();
				}
			}
			
			/* Check with directory listing to see if this address exist */
			try {
				//checking the real domain in google apps
//				String appDomain = domain;
//				appDomain = appDomain.replace("@", "");
//				AppsForYourDomainClient client = new AppsForYourDomainClient(appAdminAcc, appAdminPwd, appDomain);
//				UserFeed uf = client.retrieveAllUsers();
//				String tmpAdd = emailAddress;
//				int ind = tmpAdd.indexOf("@");
//				tmpAdd = tmpAdd.substring(0, ind);
//				/* check if exist in domain directory listing */
//				for (int j=0; j<uf.getEntries().size(); j++) {
//					UserEntry entry = uf.getEntries().get(j);
//					if (entry.getTitle().getPlainText().equalsIgnoreCase(tmpAdd)){
//						existDirectory = true;
//						break;
//					}
//				}
				EmployeeService es = new EmployeeService(); 
    			Employee employee = es.findEmployeeByColumnName("emailAddress", emailAddress.toLowerCase());
    			if(StringUtils.isNotBlank(employee.getEmailAddress())){
    				existDirectory = true;
    			}
    			
				if (existDirectory == true) {
					/* check if exist in the database */
					boolean exist = false;
					DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
					Query query = new Query(Supervisor.class.getSimpleName());
					for (Entity entity : datastore.prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
						String tmp = (String)entity.getProperty("emailAddress");
						if (tmp.equalsIgnoreCase(emailAddress)) {
							existedRegion = (String)entity.getProperty("region");
							exist = true;
						}
					}
					if (exist == false) {
						if (regionSelected==null || regionSelected.length < 0) {
							req.setAttribute("feedback", ConstantUtils.ERROR);
							req.setAttribute("message", "Please select region.");
			        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(req, resp);
							return;
						} else {
							
							SupervisorService ss = new SupervisorService();
							ss.addSupervisor(emailAddress, strRegion.replaceAll("-", " "));
							req.setAttribute("feedback", ConstantUtils.OK);
							req.setAttribute("message", "Save success.");
							getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(req, resp);
			        		return;
						}
					} else if (exist == true) {
						try {
							req.setAttribute("feedback", ConstantUtils.ERROR);
							req.setAttribute("message", "The supervisor "+emailAddress+" already exist in the database.");
			        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(req, resp);
							return;
						} catch (ServletException e) {
							log.error("AddSupervisor * doPost - error 5: " + e.getMessage());
							e.printStackTrace();
						}
					}
				} else { /* if not in directory */
					try {
						req.setAttribute("feedback", ConstantUtils.ERROR);
						req.setAttribute("message", "This email address "+emailAddress+" does not exist, please check.");
		        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(req, resp);
						return;
					} catch (ServletException e) {
						log.error("AddSupervisor * doPost - error 1: " + e.getMessage());
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				log.error("AddSupervisor doPost error: " + e.getMessage());
				e.printStackTrace();
			}
		}
	


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		log.debug(AddSupervisor.class);
		String domain = "";
		String appAdminAccount = "";
		String appAdminPassword = "";
		String emailAddress = request.getParameter("emailAddress");
		boolean existDirectory = false;
		
		
		if(StringUtils.isNotBlank(emailAddress)){
			
			Pattern pattern = Pattern.compile("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$");
			//check email pattern
	        if (!pattern.matcher(emailAddress).matches()) {
	        	
	        	try {
	        		request.setAttribute("feedback", ConstantUtils.ERROR);
	        		request.setAttribute("message", "Invalid email format");
	        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(request, response);
	        		return;
	        	} catch (Exception e1) {
	    			log.error("AddSupervisor validate email error: " + e1.getMessage());
	    			e1.printStackTrace();
	    		}
	        }
	        else{
	        	SettingService ss = new SettingService();
	    		for (Setting set : ss.getSetting()) {
	    				domain = set.getAppDomain();
	    				appAdminAccount = set.getAppAdminAcc();
	    				appAdminPassword = set.getAppAdminAccPass();
	    		}
	    		try {
	    			//checking the real domain in google apps
//	    			String appDomain = domain;
//	    			AppsForYourDomainClient client = new AppsForYourDomainClient(appAdminAccount, appAdminPassword, appDomain);
//	    			UserFeed uf = client.retrieveAllUsers();
//	    			String tmpAdd = emailAddress;
//	    			int ind = tmpAdd.indexOf("@");
//	    			tmpAdd = tmpAdd.substring(0, ind);
//	    			/* check if exist in domain directory listing */
//	    			for (int j=0; j<uf.getEntries().size(); j++) {
//	    				UserEntry entry = uf.getEntries().get(j);
//	    				if (entry.getTitle().getPlainText().equalsIgnoreCase(tmpAdd)){
//	    					existDirectory = true;
//	    					break;
//	    				}
//	    			}
	    			
	    			EmployeeService es = new EmployeeService(); 
	    			Employee employee = es.findEmployeeByColumnName("emailAddress", emailAddress.toLowerCase());
	    			if(StringUtils.isNotBlank(employee.getEmailAddress())){
	    				existDirectory = true;
	    			}
	    			
	    			String emailDomain [] = emailAddress.split("@");
	    			
	    			if (!emailDomain[1].equals(domain)) {
	    				request.setAttribute("emailAddress", emailAddress);
    					request.setAttribute("feedback", ConstantUtils.ERROR);
		        		request.setAttribute("message", "This domain "+emailDomain[1]+" does not exist, please check.");
		        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(request, response);
		        		return;
	    			}
	    			
//	    			if (existDirectory == true) {
	    				/* check if exist in the database */
	    				boolean exist = false;
	    				DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	    				Query query = new Query(Supervisor.class.getSimpleName());
	    				for (Entity entity : datastore.prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
	    					String tmp = (String)entity.getProperty("emailAddress");
	    					if (tmp.equalsIgnoreCase(emailAddress)) {
	    						exist = true;
	    					}
	    				}
	    				if (exist == true) {
	    					try {
	    						
	    						request.setAttribute("feedback", ConstantUtils.ERROR);
	    		        		request.setAttribute("message", "This supervisor "+emailAddress+" already exist in the database.");
	    		        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(request, response);
	    		        		return;
//	    						getServletConfig().getServletContext().getRequestDispatcher("/admin-add-emp-exist-in-db.jsp").forward(request, response);
//	    						return;
	    					} catch (ServletException e) {
	    						log.error("AddSupervisor * doPost - error2: " + e.getMessage());
	    						e.printStackTrace();
	    					}
	    				}
//	    			} else if (existDirectory == false) {
//	    				try {
//	    					request.setAttribute("emailAddress", emailAddress);
//	    					request.setAttribute("feedback", ConstantUtils.ERROR);
//    		        		request.setAttribute("message", "This email address "+emailAddress+" does not exist, please check.");
//    		        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(request, response);
//    		        		return;
////	    					getServletConfig().getServletContext().getRequestDispatcher("/admin-add-emp-not-exist.jsp").forward(request, response);
////	    					return;
//	    				} catch (ServletException e) {
//	    					log.error("AddSupervisor * doPost - error3: " + e.getMessage());
//	    					e.printStackTrace();
//	    				}
//	    			}
	    		} catch (Exception e1) {
	    			log.error("AddSupervisor validate email error: " + e1.getMessage());
	    			e1.printStackTrace();
	    		}
	        }
	        
		}
		else{
			request.setAttribute("feedback", ConstantUtils.ERROR);
    		request.setAttribute("message", "Mandatory field");
    		try{
    			getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(request, response);
    			return;
    		} catch (Exception e) {
    			log.error("AddSupervisor validate email error: " + e.getMessage());
    			e.printStackTrace();
    		}
    		
    		
		}
	}

}
