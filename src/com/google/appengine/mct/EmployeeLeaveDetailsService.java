package com.google.appengine.mct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.datastore.DataStoreUtil;

public class EmployeeLeaveDetailsService extends DataStoreUtil {
	
	public EmployeeLeaveDetails findEmployeeLeaveDetails(String emailAddress, String year){
		EmployeeLeaveDetails eld = new EmployeeLeaveDetails();
		Query q = new Query(EmployeeLeaveDetails.class.getSimpleName());
		Filter emailAddressFilter = new FilterPredicate("emailAddress",FilterOperator.EQUAL,emailAddress);
		Filter yearFilter = new FilterPredicate("year", FilterOperator.EQUAL,year);
		Filter filter = CompositeFilterOperator.and(emailAddressFilter, yearFilter);
		q.setFilter(filter);
		PreparedQuery pq = getDatastore().prepare(q);
		Entity entity = pq.asSingleEntity();
			eld.setName((String)entity.getProperty("name"));
			eld.setEmailAddress((String)entity.getProperty("emailAddress"));
			eld.setYear((String)entity.getProperty("year"));
			eld.setLastYearBalance((String)entity.getProperty("lastYearBalance"));
			eld.setEntitledAnnual((String)entity.getProperty("entitledAnnual"));
			eld.setEntitledCompensation((String)entity.getProperty("entitledCompensation"));
			eld.setNoPayLeave((String)entity.getProperty("noPayLeave"));
			eld.setSickLeave((String)entity.getProperty("sickLeave"));
			eld.setAnnualLeave((String)entity.getProperty("annualLeave"));
			eld.setCompensationLeave((String)entity.getProperty("compensationLeave"));
			eld.setCompassionateLeave((String)entity.getProperty("compassionateLeave"));
			eld.setBirthdayLeave((String)entity.getProperty("birthdayLeave"));
			eld.setMaternityLeave((String)entity.getProperty("maternityLeave"));
			eld.setWeddingLeave((String)entity.getProperty("weddingLeave"));
			eld.setOthers((String)entity.getProperty("others"));
			eld.setBalance((String)entity.getProperty("balance"));
			eld.setRegion((String)entity.getProperty("region"));
			eld.setId(KeyFactory.keyToString(entity.getKey()));
		
		return eld;
	}
	
	
	public EmployeeLeaveDetails findEmployeeLeaveDetailsByValue(String columnName, String value, String filterOperator){
		EmployeeLeaveDetails eld = new EmployeeLeaveDetails();
		Iterable<Entity> e = listEntities(EmployeeLeaveDetails.class.getSimpleName(), columnName, value, filterOperator);
		for(Entity entity : e){
			eld.setName((String)entity.getProperty("name"));
			eld.setEmailAddress((String)entity.getProperty("emailAddress"));
			eld.setYear((String)entity.getProperty("year"));
			eld.setLastYearBalance((String)entity.getProperty("lastYearBalance"));
			eld.setEntitledAnnual((String)entity.getProperty("entitledAnnual"));
			eld.setEntitledCompensation((String)entity.getProperty("entitledCompensation"));
			eld.setNoPayLeave((String)entity.getProperty("noPayLeave"));
			eld.setSickLeave((String)entity.getProperty("sickLeave"));
			eld.setAnnualLeave((String)entity.getProperty("annualLeave"));
			eld.setCompensationLeave((String)entity.getProperty("compensationLeave"));
			eld.setCompassionateLeave((String)entity.getProperty("compassionateLeave"));
			eld.setBirthdayLeave((String)entity.getProperty("birthdayLeave"));
			eld.setMaternityLeave((String)entity.getProperty("maternityLeave"));
			eld.setWeddingLeave((String)entity.getProperty("weddingLeave"));
			eld.setOthers((String)entity.getProperty("others"));
			eld.setBalance((String)entity.getProperty("balance"));
			eld.setRegion((String)entity.getProperty("region"));
			eld.setId(KeyFactory.keyToString(entity.getKey()));
		}
		return eld;
	}
	
	
	public String addDetails(String name, String emailAddress, String year, String lastYearBalance, String entitledAnnual, 
			String entitledCompensation, String noPayLeave, String sickLeave, String annualLeave,
			String compensationLeave, String compassionateLeave, String birthdayLeave,
			String maternityLeave, String weddingLeave, String others, String balance, String region) {
		Key employeeDetailsKey = KeyFactory.createKey(EmployeeLeaveDetails.class.getSimpleName(), "employeedetails");
		Entity empLeaveDetails = new Entity(EmployeeLeaveDetails.class.getSimpleName(),employeeDetailsKey);
		empLeaveDetails.setProperty("name", name);
		empLeaveDetails.setProperty("emailAddress", emailAddress);
		empLeaveDetails.setProperty("year", year);
		empLeaveDetails.setProperty("lastYearBalance", lastYearBalance);
		empLeaveDetails.setProperty("entitledAnnual", entitledAnnual);
		empLeaveDetails.setProperty("entitledCompensation", entitledCompensation);
		empLeaveDetails.setProperty("noPayLeave", noPayLeave);
		empLeaveDetails.setProperty("sickLeave", sickLeave);
		empLeaveDetails.setProperty("annualLeave", annualLeave);
		empLeaveDetails.setProperty("compensationLeave", compensationLeave);
		empLeaveDetails.setProperty("compassionateLeave", compassionateLeave);
		empLeaveDetails.setProperty("birthdayLeave", birthdayLeave);
		empLeaveDetails.setProperty("maternityLeave", maternityLeave);
		empLeaveDetails.setProperty("weddingLeave", weddingLeave);
		empLeaveDetails.setProperty("others", others);
		empLeaveDetails.setProperty("balance", balance);
		empLeaveDetails.setProperty("region", region);
		getDatastore().put(empLeaveDetails);
		return KeyFactory.keyToString(empLeaveDetails.getKey());   
	}
	
	
	public List<EmployeeLeaveDetails> getEmployeeLeaveDetails() {
		Query query = new Query(EmployeeLeaveDetails.class.getSimpleName());
		List<EmployeeLeaveDetails> results = new ArrayList<EmployeeLeaveDetails>();
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			EmployeeLeaveDetails eld = new EmployeeLeaveDetails();
			eld.setName((String)entity.getProperty("name"));
			eld.setEmailAddress((String)entity.getProperty("emailAddress"));
			eld.setYear((String)entity.getProperty("year"));
			eld.setLastYearBalance((String)entity.getProperty("lastYearBalance"));
			eld.setEntitledAnnual((String)entity.getProperty("entitledAnnual"));
			eld.setEntitledCompensation((String)entity.getProperty("entitledCompensation"));
			eld.setNoPayLeave((String)entity.getProperty("noPayLeave"));
			eld.setSickLeave((String)entity.getProperty("sickLeave"));
			eld.setAnnualLeave((String)entity.getProperty("annualLeave"));
			eld.setCompensationLeave((String)entity.getProperty("compensationLeave"));
			eld.setCompassionateLeave((String)entity.getProperty("compassionateLeave"));
			eld.setBirthdayLeave((String)entity.getProperty("birthdayLeave"));
			eld.setMaternityLeave((String)entity.getProperty("maternityLeave"));
			eld.setWeddingLeave((String)entity.getProperty("weddingLeave"));
			eld.setOthers((String)entity.getProperty("others"));
			eld.setBalance((String)entity.getProperty("balance"));
			eld.setRegion((String)entity.getProperty("region"));
			eld.setId(KeyFactory.keyToString(entity.getKey()));
			results.add(eld);
		}
		/* Sort by name */
		Collections.sort(results, new SortEmpLeaveDetails());
		return results;
	}


	public void deleteEmployeeLeaveDetails(String id) {
		Transaction txn = getDatastore().beginTransaction();
		try{
			Filter historyFilter = new FilterPredicate("__key__",
                    FilterOperator.EQUAL,
                    KeyFactory.stringToKey(id));
			Query query = new Query(EmployeeLeaveDetails.class.getSimpleName()).setFilter(historyFilter);
			Entity entity = getDatastore().prepare(query).asSingleEntity();
			getDatastore().delete(entity.getKey());
			txn.commit();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}


	public void updateEmployeeLeaveDetails(String name, String emailAddress, String year, String lastYearBalance, String entitledAnnual, 
			String entitledCompensation, String noPayLeave, String sickLeave, String annualLeave,
			String compensationLeave, String compassionateLeave, String birthdayLeave,
			String maternityLeave, String weddingLeave, String others, String balance, String region) throws EntityNotFoundException {
			EmployeeLeaveDetails empLeaveDetails = findEmployeeLeaveDetails(emailAddress,year);
			Entity eld = findEntity(empLeaveDetails.getId());
						eld.setProperty("name", name);
						eld.setProperty("emailAddress", emailAddress);
						eld.setProperty("year", year);
						eld.setProperty("lastYearBalance", lastYearBalance);
						eld.setProperty("entitledAnnual", entitledAnnual);
						eld.setProperty("entitledCompensation", entitledCompensation);
						eld.setProperty("noPayLeave", noPayLeave);
						eld.setProperty("sickLeave", sickLeave);
						eld.setProperty("annualLeave", annualLeave);
						eld.setProperty("compensationLeave", compensationLeave);
						eld.setProperty("compassionateLeave", compassionateLeave);
						eld.setProperty("birthdayLeave", birthdayLeave);
						eld.setProperty("maternityLeave", maternityLeave);
						eld.setProperty("weddingLeave", weddingLeave);
						eld.setProperty("others", others);
						eld.setProperty("balance", balance);
						eld.setProperty("region", region);
						getDatastore().put(eld);
					
		}
}
