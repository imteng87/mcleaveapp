<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<%
String holDate = (String) request.getAttribute("holDate"); if (holDate == null) { holDate = ""; }

String desc = (String) request.getAttribute("desc"); if (desc == null) { desc = ""; }
	
String region = (String) request.getAttribute("region"); if (region == null) { region = ""; }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <style>
      undefined
    </style>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
    <script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/js/common.js"></script>
		<link rel="stylesheet" href="media/themes/ui-lightness/jquery-ui-1.8.4.custom.css" />
		<script src="script/jquery-ui.js"></script>
		
		<script language="JavaScript">
// 			var running = 0;
			function Go() {
				var f = document.forms[0];	
				if (f.holDate.value == "" || f.desc.value == "" 
						|| f.region.value == "" || f.region.value == "Default") {
					/* alert("Please make sure all fields are filled."); */
					$('#myModal').modal('show')
					return false;
				}
				return true;
// 				f.submit();
// 				running++;
			}
			function cmd_parm(msg) { 
				var f = document.forms[0];
				f.cmd.value = msg;
				if (f.cmd.value == "End"){
					if (f.holDate.value == "" || f.desc.value == "") {
						/* alert("Please make sure the field is filled."); */
						$('#myModal').modal('show')
						return;
					}
				}
				f.submit();
			}
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
  			function clearForm() {
  				var f_elements = document.forms[0].elements;
  				for (i = 0; i < f_elements.length; i++) {
    				field_type = f_elements[i].type.toLowerCase();
    				switch (field_type) {
    					case "text":
    					case "password":
						case "textarea":
    					case "hidden":
    						f_elements[i].value = "";
        					break;
    					case "radio":
    					case "checkbox":
        					if (f_elements[i].checked) {
            					f_elements[i].checked = false;
        					}
        					break;
    					case "select-one":
    						f_elements[i].selectedIndex = 0;
    						break;
    					case "select-multi":
        					f_elements[i].selectedIndex = -1;
        					break;
    					default:
        					break;
    				}
				}
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
		<script type="text/javascript">
        $(document).ready(function () {
        	
        	$("#holDate").datepicker({
        		changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy"
            });
        	
        	$("#add-holiday").on("click",function(){
        		
            	if(Go()){
            		$(".span9").block({
    	    		 	showOverlay: true, 
    	    	        centerY: true, 
    	    	        css: { 
    	    	            width: '200px',  
    	    	            border: 'none', 
    	    	            padding: '15px', 
    	    	            backgroundColor: '#000', 
    	    	            '-webkit-border-radius': '10px', 
    	    	            '-moz-border-radius': '10px', 
    	    	            opacity: .6, 
    	    	            color: '#fff' 
    	    	        }, 
    	    			message: '<font face="arial" size="4">Loading ...</font>'
    	    			
    	    		});
            		
            		var f = document.forms[0];
    	    		
    	    		$.ajax({
    	    			type: "POST",
    	    			url: "/AddHoliday",
    	    			data: {holDate:f.holDate.value, desc:f.desc.value,
    	    				region:f.region.value},
    	    			success: function(response) {
    	    				$(".span9").unblock();
    	                	
    	                	var code = $("#feedback", response).text();
    	                	var message = $("#message", response).text();

    	                	$("#msg").fadeIn().text(message);
    	                	
    	                	return false;
    	    			}
    	    		});
            	}
            	
    				
            	});
        	
        });
        </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
    <jsp:include page="account-menu.jsp"></jsp:include>
    <!-- </div> -->
    </div>
    <div class="span9" >
				<h5 id="Regional Holiday">ADD REGIONAL HOLIDAY</h5>
				<hr></hr>
				<div id="msg" class="error-msg"></div>
				<form name="Add" method="post" action="AddHoliday">
					<input type=hidden name=cmd value="">
					<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
					<jsp:setProperty name="jspbeans" property="*" />
					<table cellpadding="10" border="0">
						<tr>
							<td colspan="3" style="padding-left:0px; color:black;"><i>Please take note that once the holiday is added, the calendar will be updated too.</i></td>
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;padding-bottom:16px; color:black;" width="50px;">Date</td>
							<td width="230px">
								<div class="textbox">
									<input type="text" id="holDate" name="holDate" value="<%=holDate%>" readonly/>
								</div>
							</td>
							<td>
							</td>
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;padding-bottom:16px; color:black;">Description</td>
							<td>
								<textarea name="desc" id="desc" value="<%=desc%>" rows="8" cols="27"></textarea>
							</td>
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;padding-bottom:16px; color:black;">Region</td>
							<td>
								<div class="dropdown">
									<select name="region">
										<%=jspbeans.listRegion()%>
									</select>
									<script>
										this.document.forms[0].region.value="<%=region%>"; 
									</script>
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<a href="#" class="btn btn-primary" id="add-holiday">Save</a>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="#" onClick="javascript:clearForm()" class="btn btn-danger">Reset</a>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!-- 								<a href="admin-new-holidays.jsp"><img src="images/End_Button.png" style="border:none;" -->
<!-- 								onmouseover="this.src='images/End_Hover.png'" onmouseout="this.src='images/End_Button.png'"/></a> -->
							</td>
						</tr>
					</table>
				</form>
				
			</div>
			</div></div>
			
			<!-- Modal -->
				<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				    <h4 id="myModalLabel">Add Regional Holiday</h4>
				  </div>
				  <div class="modal-body">
				    <p>Please make sure all fields are filled.</p>
				  </div>
				  <div class="modal-footer">			    
				    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
				  </div>
				</div>
	</body>
<html>