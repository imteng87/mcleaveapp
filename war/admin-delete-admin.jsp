<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
      undefined
    </style>
    <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		<script language="JavaScript">
			
			function Go() {
		    	if( $('form[name=Delete] input[name=delAdlist]:checked').length > 0)
		    	{
		        	$('form[name=Delete]').submit();
		    	}
		    	else
		    	{
		        	/* alert("Please select at least one to delete"); */
		        	$('#myModal').modal('show')
		    	}
			} 
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
  			function clearForm() {
  				var f_elements = document.forms[0].elements;
  				for (i = 0; i < f_elements.length; i++) {
    				field_type = f_elements[i].type.toLowerCase();
    				switch (field_type) {
    					case "text":
    					case "password":
						case "textarea":
    					case "hidden":
    						f_elements[i].value = "";
        					break;
    					case "radio":
    					case "checkbox":
        					if (f_elements[i].checked) {
            					f_elements[i].checked = false;
        					}
        					break;
    					case "select-one":
    						f_elements[i].selectedIndex = 0;
    						break;
    					case "select-multi":
        					f_elements[i].selectedIndex = -1;
        					break;
    					default:
        					break;
    				}
				}
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
		
        <script type="text/javascript">
        $(document).ready(function () {        	 
        	 
            	$('#adminList').dataTable( {
            		"bServerSide": true,
            		"sAjaxSource": "/DeleteAdministrator",
            		"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            		"sPaginationType": "bootstrap",
            		"oLanguage": {
            			"sLengthMenu": "_MENU_ records per page"
            		}
				});
            	
            	
            	
            		$("#delete-admin").on("click",function(){
            			
            			if( $('form[name=Delete] input[name=delAdlist]:checked').length > 0)
        		    	{
            				var delAdlist = new Array();
            				
                		$("input[name=delAdlist]:checked").each(function() { 
                			delAdlist.push($(this).val());
                	    });
                		
                		$(".span9").block({
                		 	showOverlay: true, 
                	        centerY: true, 
                	        css: { 
                	            width: '200px',  
                	            border: 'none', 
                	            padding: '20px', 
                	            backgroundColor: '#000', 
                	            '-webkit-border-radius': '10px', 
                	            '-moz-border-radius': '10px', 
                	            opacity: .6, 
                	            color: '#fff' 
                	        }, 
                			message: '<font face="arial" size="4">Loading ...</font>'
                			
                		});
                		
                		$.ajax({
                			type: "POST",
                			url: "/DeleteAdministrator",
                			data: {delAdlist:delAdlist},
                			success: function(response) {
                				$(".span9").unblock();
                            	
                            	window.location.href = "admin-delete-admin.jsp";
                            	
                            	return false;
                			}
                		});

        		    	}
        		    	else
        		    	{
        		        	/* alert("Please select at least one to delete"); */
        		        	$('#myModal').modal('show')
        		    	}

            		});
		    	
            	
        });
        </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
    <jsp:include page="account-menu.jsp"></jsp:include>
    <!-- </div> -->
    </div>
    <div class="span9">
				<h5 id="Admin">ADMINISTRATOR</h5>
				<hr></hr>
				<span>
					<a href="admin-add-admin.jsp" class=" btn btn-info">
					<i class="icon-black icon-pencil"></i> Add Administrator</a>
				</span>
				<br></br>		
				<!-- <table style="width: 100%;">					
					<tr>
						<td style="padding-bottom:18px;" >
							<a href="admin-add-admin.jsp" class="pull-right btn btn-info" ><i class="icon-black icon-pencil"></i> 
							Add Administrator</a>							
						</td>
					</tr>
				</table> -->
				
				<div id="msg" class="error-msg"></div>
				<form name="Delete" method="post" action="DeleteAdministrator">
					<table cellpadding="0" cellspacing="0" border="0" id="adminList" class="table table-striped table-bordered" >
					<thead id="headed">
					<tr>
						<th align="center" title="Select">Select</th>
						<th align="center" title="Email Address">Email Address</th> 
						</tr>
					</thead>
					<tbody></tbody>
					</table>
					<table id="footed">
						<tr>
							<td colspan="2">
								<a href="#" class="btn btn-primary" id="delete-admin">Delete</a>
								&nbsp;&nbsp;&nbsp;
								<a href="#" class="btn btn-danger" onClick="javascript:clearForm()">Reset</a>
							</td>
						</tr>
					</table>
				</form>
			</div></div></div>
			
			<!-- Modal -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			    <h4 id="myModalLabel">Delete Administrator</h4>
			  </div>
			  <div class="modal-body">
			    <p>Please select at least one Admin to delete.</p>
			  </div>
			  <div class="modal-footer">			    
			    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			  </div>
			</div>
	</body>
</html>