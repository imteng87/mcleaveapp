<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<script language="javaScript" type="text/javascript" src="script/calendar.js"></script>
   		<link href="css/calendar.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<script language="JavaScript">
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
	</head>
	<body>
		<div class="wrapper">
	        <div id="head">
			</div>
			<div id="content">
				<h4>Leave Request Action Status</h4>
				<br/>
				<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.RequestAction"></jsp:useBean>
				<jsp:setProperty name="jspbeans" property="*"/>
				<table cellpadding="5">
					<tr>
	    				<td rowspan="2" style="padding-left:0px;"><img src="images/thankyou.png" width="70px"/></td>
	    				<td style="color:black;"><%=jspbeans.leaveReqActionMsg()%></td>
					</tr>
					<tr>
						<td style="color:red;"><%=jspbeans.leaveReqActionErrMsg()%></td>
					</tr>
				</table>
			</div>
			<div id="menu">
				<ul>
					<li><a href="admin-new-admin.jsp">ADMIN</a></li>
					<li><a href="admin-new-emp.jsp">EMPLOYEE</a></li>
					<li><a href="admin-new-supervisor.jsp">SUPERVISOR</a></li>
					<li><a href="admin-new-region.jsp">REGIONS</a></li>
					<li><a href="admin-new-holidays.jsp">REGIONAL HOLIDAYS</a></li>
					<li><a href="admin-new-history.jsp">HISTORY</a></li>
					<li><a href="admin-new-leave-request.jsp">LEAVE REQUESTS</a></li>
					<li><a href="admin-new-emp-leave-details.jsp">EMPLOYEES LEAVE DETAILS</a></li>
					<li><a href="admin-new-emp-leave-queue.jsp">PENDING LEAVE QUEUE</a></li>
					<li><a href="admin-new-mct-leave.jsp">MASTER CONCEPT LEAVE</a></li>
					<li><a href="admin-new-app-rej-req.jsp">APPROVE / REJECT REQUEST</a></li>
					<ul id="anrBar" style="list-style: none; margin-bottom: 0px; padding: 0 0 0 0;">
						<li style="background:url('images/Arrow_Red.png');"><a href="admin-request-action.jsp">&nbsp;&nbsp; Approve / Reject Leave Request</a></li>
					</ul>
					<li><a href="admin-new-settings.jsp">SYSTEM SETTINGS</a></li>
					<li><a href="log-out.jsp">LOG OUT</a></li>
				</ul>                                                   
			</div>
	        <div class="push"></div>
	    </div>
	    <div class="footer"></div>
	</body>
<html>