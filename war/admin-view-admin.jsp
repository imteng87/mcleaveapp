<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <style>
      undefined
    </style>
    <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
<!--     <link href="media/css/demo_page.css" rel="stylesheet" type="text/css" /> -->
<!--         <link href="media/css/demo_table.css" rel="stylesheet" type="text/css" /> -->
<!--         <link href="media/css/demo_table_jui.css" rel="stylesheet" type="text/css" /> -->
<!--         <link href="media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" /> -->
<!--         <link href="media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" media="all" /> -->
<!--         <link href="media/themes/smoothness/jquery-ui-1.8.4.custom.css" rel="stylesheet" type="text/css" media="all" /> -->
        <script src="media/js/jquery.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<!--  <script type="text/javascript" charset="utf-8" language="javascript" src="http://www.datatables.net/release-datatables/media/js/jquery.js"></script> -->
<!-- 		<script type="text/javascript" charset="utf-8" language="javascript" src="http://www.datatables.net/release-datatables/media/js/jquery.dataTables.js"></script> -->
<!--     <script src="http://www.datatables.net/media/blog/bootstrap_2/DT_bootstrap.js"></script> -->
   <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
   <script src="assets/js/common.js"></script>
		<script language="JavaScript">
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
		
        <script type="text/javascript">
        $(document).ready(function () {
        	
        	
     
//             	$("#adminList").dataTable({
//                     "bServerSide": true,
//                     "sAjaxSource": "/ViewAdministrator",
//                     "bProcessing": true,
//                     "sPaginationType": "full_numbers",
//                     "bJQueryUI": true
//                 });
            	
            	$('#adminList').dataTable( {
            		"bServerSide": true,
            		"sAjaxSource": "/ViewAdministrator",
            		"sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
            		"sPaginationType": "bootstrap",
            		"oLanguage": {
            			"sLengthMenu": "_MENU_ records per page"
            		}
				});
            	
        });
        </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <div class="well sidebar-nav">
    <jsp:include page="account-menu.jsp"></jsp:include>
    </div>
    </div>
    <div class="span9">
				<h5 id="View Admin">VIEW ADMINISTRATOR</h5>
				<hr></hr>
					<table cellpadding="0" cellspacing="0" border="0" id="adminList"
					 class="table table-striped table-bordered" >
						<thead id="headed">
							<tr>
							<th align="center" title="Email Address">Email Address</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					</div>
			</div>
			</div>
	</body>
</html>