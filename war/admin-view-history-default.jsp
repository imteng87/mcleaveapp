<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<%
String cri_region = (String) request.getAttribute("cri_region"); if (cri_region == null) { cri_region = ""; }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="utf-8" http-equiv="encoding"/>
		<title>Master Concept</title>
		<script language="javaScript" type="text/javascript" src="script/calendar.js"></script>
   		<link href="css/calendar.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<script language="JavaScript">
			function toggle(id) {
    			var n = document.getElementById(id);
    			n.style.display =  (n.style.display != 'none' ? 'none' : '' );
  			}
			function cmd_parm() { 
				var f = document.forms[0];
				f.submit();
			}
			function cmd_import(msg) {
				var f = document.forms[0];
				f.cmdImport.value = msg;
				f.submit();
			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
		<link href="media/css/demo_page.css" rel="stylesheet" type="text/css" />
        <link href="media/css/demo_table.css" rel="stylesheet" type="text/css" />
        <link href="media/css/demo_table_jui.css" rel="stylesheet" type="text/css" />
        <link href="media/css/jquery.dataTables.css" rel="stylesheet" type="text/css" media="all" />
        <link href="media/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" media="all" />
        <link href="media/themes/smoothness/jquery-ui-1.8.4.custom.css" rel="stylesheet" type="text/css" media="all" />
        <script src="media/js/jquery.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function () {
        	
        	$("#headed").hide();
        	
            $("#region").on("change", function (){
            	
            	var region = $(this).val();
            	
            	$("#headed").show();
            	
            	$("#historyList").dataTable({
                    "bServerSide": true,
                    "sAjaxSource": "/ViewHistory",
                    "bProcessing": true,
                    "bRetrieve": true,
                    "fnServerParams": function ( aoData ) {
                        aoData.push( { "name":"cri_region", "value":region } );
                      },
                    "sPaginationType": "full_numbers",
                    "bJQueryUI": true
                });
            	
            	return;
            });
        });
        </script>
	</head>
	<body>
		<div class="wrapper">
	        <div id="head"></div>
			<div id="content">
				<h5>VIEW LEAVE HISTORY</h5>
				<hr></hr>
				<form name="View" method="post" action="ViewHistory">
				<input type="hidden" name="cmdImport" value=""/>
				<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
				<jsp:setProperty name="jspbeans" property="*"/>
				<table width="800px" cellpadding="5" style="color:black; border-top:1px solid #ccc;" border="0">
<!-- 					<tr> -->
<!-- 						<td colspan="5" style="padding-left:0px; color:black;"><i>To import the leave history from Google Docs, click on the Import button.</i></td> -->
<!-- 						<td> -->
<!-- 							<a href="#"><img src="images/Import.png" style="border:none;" onClick="javascript:cmd_import('Import');" -->
<!-- 								onmouseover="this.src='images/Import_Hover.png'" onmouseout="this.src='images/Import.png'"></a> -->
<!-- 						</td> -->
<!-- 					</tr> -->
					<tr>
						<td colspan="5" style="padding-left:0px; color:black;"><i>To view, please select the region to narrow down your search.</i></td>
					</tr>
					<tr>
						<td align="left" style="padding-left:0px;padding-bottom:15px;" width="40px">Region</td>
						<td colspan="2">
							<div class="dropdown">
								<select name="cri_region" id="region">
									<%=jspbeans.listRegion()%>
								</select>
								<script>
									this.document.forms[0].cri_region.value="<%=cri_region%>"; 
								</script>
							</div>
						</td>
					</tr>
				</table>
				<div id="container">
					<div id="demo_jui">
					<table id="historyList" class="display" cellpadding="0" cellspacing="0" border="0" style="font-size:12px;" >
					<thead id="headed">
						<tr>
						<th  align="center" title="Time">Time</th>
						<th align="center" title="Employee">Employee</th>
						<th  align="center" title="Number of Days">Number of Days</th>
						<th align="center" title="Start Date">Start Date</th>
						<th align="center" title="End Date">End Date</th>
						<th  align="center" title="Supervisor">Supervisor</th>
						<th align="center" title="Leave Type">Leave Type</th>
						<th  align="center" title="Remark">Remark</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
					</div>
				</div>
				
				</form>
			</div>
			<div id="menu">
				<ul>
					<li><a href="admin-new-admin.jsp">ADMIN</a></li>
					<li><a href="admin-new-emp.jsp">EMPLOYEE</a></li>
					<li><a href="admin-new-supervisor.jsp">SUPERVISOR</a></li>
					<li><a href="admin-new-region.jsp">REGIONS</a></li>
					<li><a href="admin-new-holidays.jsp">REGIONAL HOLIDAYS</a></li>
					<li><a href="admin-new-history.jsp">HISTORY</a></li>
					<ul id="hisBar" style="list-style: none; margin-bottom: 0px; padding: 0 0 0 0;">
						<li style="background:url('images/Arrow_Red.png');"><a href="admin-view-history.jsp">&nbsp;&nbsp; View Leave History</a></li>
						<li><a href="admin-delete-history.jsp">&nbsp;&nbsp; Delete Leave History</a></li>
					</ul>
					<li><a href="admin-new-leave-request.jsp">LEAVE REQUESTS</a></li>
					<li><a href="admin-new-emp-leave-details.jsp">EMPLOYEES LEAVE DETAILS</a></li>
					<li><a href="admin-new-emp-leave-queue.jsp">PENDING LEAVE QUEUE</a></li>
					<li><a href="admin-new-mct-leave.jsp">MASTER CONCEPT LEAVE</a></li>
					<li><a href="admin-new-app-rej-req.jsp">APPROVE / REJECT REQUEST</a></li>
					<li><a href="admin-new-settings.jsp">SYSTEM SETTINGS</a></li>
					<li><a href="log-out.jsp">LOG OUT</a></li>
				</ul>                                                   
			</div>
	        <div class="push"></div>
	    </div>
	    <div class="footer"></div>
	</body>
</html>